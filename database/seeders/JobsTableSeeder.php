<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Job;
use DB;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            Job::truncate();
            
    
            Job::create(['name' => 'kotlovan', 'roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])]);
            

            Job::create(['name' => 'fundament', 'roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])]);
            
            Job::create(['name' => 'karkas', 'roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])]);
            
            Job::create(['name' => 'steny', 'roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])]);
            
            Job::create(['name' => 'krovlya', 'roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])]);
            
            Job::create(['name' => 'fasad', 'roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])]);
            
            Job::create(['name' => 'lift', 'roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])]);
            
            Job::create(['name' => 'oknaidveri', 'roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])]);
            
            Job::create(['name' => 'vnutrennayaotdelka', 'roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])]);
            
            Job::create(['name' => 'santehnika', 'roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])]);
            
            Job::create(['name' => 'elektrosnabjeniye', 'roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])]);
            
            Job::create(['name' => 'gazonabjeniye', 'roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])]);
            
            Job::create(['name' => 'otopleniye', 'roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])]);
            
            Job::create(['name' => 'blagoustroystvo', 'roles' => json_encode(['admin','zam','prorab','tehnadzor','otk','inj','masteruchastka'])]);
            
        }
    }
}
