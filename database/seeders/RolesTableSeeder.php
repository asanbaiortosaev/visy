<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();

        Role::create(['name' => 'admin']);
        Role::create(['name' => 'zam']);
        Role::create(['name' => 'prorab']);
        Role::create(['name' => 'tehnadzor']);
        Role::create(['name' => 'otk']);
        Role::create(['name' => 'fin']);
        Role::create(['name' => 'inj']);
        Role::create(['name' => 'yur']);
        Role::create(['name' => 'snabj']);
        Role::create(['name' => 'masteruchastka']);
        // Role::create(['name' => 'user']);
    }
}
