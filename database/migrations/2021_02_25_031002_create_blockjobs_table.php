<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlockjobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blockjobs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('block_id')->constrained();
            $table->string('block_name');
            $table->foreignId('job_id')->constrained();
            $table->string('job_name');
            $table->foreignId('subjob_id')->constrained();
            $table->string('subjob_name');
            $table->json('roles')->nullable();
            $table->boolean('status')->default(0);
            $table->integer('etaj')->nullable();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('deadline')->nullable();
            $table->timestamp('finish_date')->nullable();
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blockjobs');
    }
}
