<?php

use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Auth\LoginController;
// use App\Http\Controllers\API\ApiAuthController;   
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\BlockController;
use App\Http\Controllers\BlockjobController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\JobController;
use App\Http\Controllers\SubjobController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TaskCommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {return view('home');})->name('home');

Route::get('/login',[LoginController::class, 'index'])->name('login');
// Route::post('/login/api',[ApiAuthController::class, 'login']);
Route::post('/login',[LoginController::class, 'store']);

Route::post('/logout',[LogoutController::class, 'store'])->name('logout');
Route::get('/dashboard',[DashboardController::class, 'index'])->name('dashboard');

Route::namespace('App\Http\Controllers\Admin')->prefix('admin')->name('admin.')->middleware('admin')->group(function(){
    Route::resource('/users', UserController::class)->except(['show','destroy']);
});

Route::get('/blocks', [BlockController::class,'index'])->name('blocks.index');
Route::get('/blocks/create', [BlockController::class,'create'])->name('blocks.create')->middleware('admin');
Route::post('/blocks', [BlockController::class,'store'])->middleware('admin');
Route::get('/blocks/{id}/edit', [BlockController::class,'edit'])->name('blocks.edit')->middleware('admin');
Route::get('/blocks/{id}/addsubjobform', [BlockController::class,'addsubjobform'])->name('blocks.addsubjobform')->middleware('admin');
Route::post('/blocks/{id}/addsubjob', [BlockController::class,'addsubjob'])->name('blocks.addsubjob')->middleware('admin');
Route::put('/blocks/{id}', [BlockController::class,'update'])->name('blocks.update')->middleware('admin');
Route::get('/blocks/{id}', [BlockController::class,'show'])->name('blocks.show');

Route::get('/projects', [ProjectController::class,'index'])->name('projects.index');
Route::get('/projects/create', [ProjectController::class,'create'])->name('projects.create')->middleware('admin');
Route::post('/projects', [ProjectController::class,'store'])->middleware('admin');
Route::get('/projects/{id}/edit', [ProjectController::class,'edit'])->name('projects.edit')->middleware('admin');
Route::put('/projects/{id}', [ProjectController::class,'update'])->name('projects.update')->middleware('admin');
Route::get('/projects/{id}', [ProjectController::class,'show'])->name('projects.show');

Route::get('/jobs', [JobController::class,'index'])->name('jobs.index');
Route::get('/jobs/create', [JobController::class,'create'])->name('jobs.create');
Route::post('/jobs', [JobController::class,'store']);
Route::get('/jobs/{id}/edit', [JobController::class,'edit'])->name('jobs.edit');
Route::put('/jobs/{id}', [JobController::class,'update'])->name('jobs.update');

Route::get('/subjobs', [SubjobController::class,'index'])->name('subjobs.index');
Route::get('/subjobs/create', [SubjobController::class,'create'])->name('subjobs.create');
Route::post('/subjobs', [SubjobController::class,'store']);
Route::get('/subjobs/{id}/edit', [SubjobController::class,'edit'])->name('subjobs.edit');
Route::put('/subjobs/{id}', [SubjobController::class,'update'])->name('subjobs.update');

Route::get('/blockjobs/{id}/show', [BlockjobController::class,'show'])->name('blockjobs.show');
Route::get('/blockjobs/{id}/activate', [BlockjobController::class,'activate'])->name('blockjobs.activate')->middleware('admin');
Route::post('/blockjobs/{id}', [BlockjobController::class,'storeactivate'])->name('blockjobs.storeactivate')->middleware('admin');

Route::get('/blockjobs/{id}/tasks/create', [TaskController::class, 'create'])->name('tasks.create')->middleware('admin');
Route::post('/blockjobs/{id}/tasks', [TaskController::class, 'store'])->name('tasks.store')->middleware('admin');
Route::get('/tasks/{id}/edit', [TaskController::class, 'edit'])->name('tasks.edit')->middleware('admin');
Route::put('/tasks/{id}', [TaskController::class,'update'])->name('tasks.update')->middleware('admin');

Route::get('taskcomments/{id}/create', [TaskCommentController::class, 'create'])->name('taskcomments.create');
Route::post('taskcomments/{id}', [TaskCommentController::class, 'store'])->name('taskcomments.store');
Route::get('taskcomments/{id}/edit', [TaskCommentController::class, 'edit'])->name('taskcomments.edit');
Route::put('taskcomments/{id}', [TaskCommentController::class, 'update'])->name('taskcomments.update');