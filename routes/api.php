<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\ApiAuthController;
use App\Http\Controllers\API\ApiTestController;
use App\Http\Controllers\API\ApiUserController;    
use App\Http\Controllers\API\ApiProjectController;    
use App\Http\Controllers\API\ApiBlockController;    
use App\Http\Controllers\API\ApiJobController;    
use App\Http\Controllers\API\ApiSubjobController;    
use App\Http\Controllers\API\ApiBlockjobController;    
use App\Http\Controllers\API\ApiTaskController;    
use App\Http\Controllers\API\ApiTaskCommentController;    

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {

    Route::post('login', [ApiAuthController::class,'login']); //ok  
    Route::post('logout', [ApiAuthController::class,'logout']); //ok
    
});

Route::group(['middleware' => 'jwt'], function () {
        
    Route::get('projects',[ApiProjectController::class,'index']); //ok
    Route::get('projects/{id}',[ApiProjectController::class,'show']); //ok

    Route::get('/blockjobs/{id}/show', [ApiBlockjobController::class,'show']); //ok

    Route::post('taskcomments/{id}', [ApiTaskCommentController::class, 'store']); //ok
    Route::put('taskcomments/{id}', [ApiTaskCommentController::class, 'update']); //ok

        Route::group(['middleware' => 'apiadmin'], function () {

            Route::get('users',[ApiUserController::class,'index']); //ok
            Route::post('users',[ApiUserController::class,'store']);  //ok
            Route::put('users/update/{id}',[ApiUserController::class,'update']);  //ok
            // Route::delete('users/{id}',[ApiUserController::class,'destroy']);
        
            Route::get('blocks',[ApiBlockController::class,'index']); //ok
            Route::post('blocks',[ApiBlockController::class,'store']); //ok
            Route::put('blocks/update/{id}',[ApiBlockController::class,'update']); //ok
            Route::get('/blocks/{id}', [ApiBlockController::class,'show']); //ok
            Route::post('/blocks/{id}/addsubjob', [ApiBlockController::class,'addsubjob']); // ok
            
            Route::get('jobs', [ApiJobController::class,'index']); //ok
            Route::post('jobs', [ApiJobController::class,'store']); // ok
            Route::put('jobs/{id}', [ApiJobController::class,'update']); //ok
        
            Route::get('/subjobs', [ApiSubjobController::class,'index']); //ok
            Route::post('/subjobs', [ApiSubjobController::class,'store']);  //ok
            Route::put('/subjobs/{id}', [ApiSubjobController::class,'update']); //ok
        
            Route::post('projects',[ApiProjectController::class,'store']); //ok
            Route::put('projects/update/{id}',[ApiProjectController::class,'update']); //ok
        
            Route::post('/blockjobs/{id}', [ApiBlockjobController::class,'storeactivate']); //ok
        
            Route::post('/blockjobs/{id}/tasks', [ApiTaskController::class, 'store']); //ok
            Route::put('/tasks/{id}', [ApiTaskController::class,'update']); //ok
        });

});




