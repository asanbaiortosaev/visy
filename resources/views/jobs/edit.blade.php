@extends('layouts.app')

@section('content')

<div class="flex justify-center">
    <div class="w-4/12 bg-white p-6 rounden-lg">
        @if (session('status'))
            <div class="bg-red-500 p-4 rounded-lg mb-6 text-white text-center">
                {{ session('status') }}
            </div>
        @endif

        <div class="mb-4 text-center">
                
        EDIT JOB
                
        </div>   

        <form action="{{ route('jobs.update', $job) }}" method="post">
            @csrf
            @method('PUT')
            
            <div class="mb-4">
                <label for="name" class="sr-only"> Name </label>
                <input type="text" name="name" id="name" placeholder="New Job Name"
                class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('name') border-red-500 @enderror" value="{{ $job->name }}">
                @error('name')
                    <div class="text-red-500 mt-2 text-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div>   
            
            

            
            <div class="mb-4">
                <label for="roles" class=""> Roles </label>
                <div class="">
                    @foreach ($roles as $role)
                        <div class="form-check">
                            <input type="checkbox" name="roles[]" value=" {{ $role->name }} "
                            @if (in_array($role->name, $var)) { checked : '' } @endif >
                            <label> {{ $role->name }} </label>                        
                        </div>
                    @endforeach
                </div>
            </div>
            
            <div>
                <button type="submit" class="bg-blue-500 text-white px-4 py-3
                rounded font-medium w-full focus:outline-none hover:bg-indigo-600 rounded"> Update </button>
            </div>           
        </form>
    </div>
</div>

@endsection
