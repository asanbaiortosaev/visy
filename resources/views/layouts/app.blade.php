<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vision Group</title>
    
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" >


</head>
<body class="bg-gray-200">
    <nav class="p-6 bg-blue-500 bg-opacity-75 flex justify-between mb-6">
        <ul class="flex items-center">
            <!-- <li>
                <a href="/" class="p-3">Home</a>
            </li> -->
            @auth
            <li>
                <a href="{{ route('dashboard') }}"class="p-3 focus:outline-none hover:bg-indigo-600 rounded">Dashboard</a>
            </li>
            

            @if (Auth::user()->isAdmin())
            <li>
                <a href="{{ route('admin.users.index') }}" class ="p-3 focus:outline-none hover:bg-indigo-600 rounded">
                    User Managment
                </a>
            </li>
            @endif
            
            <li>
                <a href="{{ route('projects.index') }}" class ="p-3 focus:outline-none hover:bg-indigo-600 rounded">Projects</a>            
            </li>
            @if (Auth::user()->isAdmin())
            <li>
                <a href="{{ route('blocks.index') }}" class ="p-3 focus:outline-none hover:bg-indigo-600 rounded">Blocks</a>            
            </li>
            <li>
                <a href="{{ route('jobs.index') }}" class ="p-3 focus:outline-none hover:bg-indigo-600 rounded">Jobs</a>            
            </li>
            <li>
                <a href="{{ route('subjobs.index') }}" class ="p-3 focus:outline-none hover:bg-indigo-600 rounded">Subjobs</a>            
            </li>
            @endif

            @endauth

        </ul>


        <ul class="flex items-center">
            @auth
            <li>
                {{auth()->user()->name}}            
            </li>

            
            <li>
                <form action="{{ route('logout') }}" method="post" class="p-3 inline">
                    @csrf
                    <button type="submit" class ="p-3 focus:outline-none hover:bg-indigo-600 rounded">Logout</button>            
                </form>
            </li>
            @endauth

            @guest
            
            <li>
                <a href="{{ route('login') }}" class ="p-3 focus:outline-none hover:bg-indigo-600 rounded">Login</a>            
            </li>
            
            @endguest
        </ul>
    </nav>
    @yield('content')
</body>
</html>