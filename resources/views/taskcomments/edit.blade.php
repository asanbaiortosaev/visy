@extends('layouts.app')
@section('content')

<div class="flex justify-center">
    <div class="w-4/12 bg-white p-6 rounden-lg">
        @if (session('status'))
            <div class="bg-red-500 p-4 rounded-lg mb-6 text-white text-center">
                {{ session('status') }}
            </div>
        @endif

        <div class="mb-4 text-center">
                
        EDIT COMMENT
                
        </div>   

        <form action="{{ route ('taskcomments.update', $comment->id ) }}" method="post">
            @csrf
            @method('PUT')
            
            <div class="mb-4">
                <label for="body" class="sr-only"> TaskComment </label>
                <input type="text" name="body" id="body" placeholder="enter comment"
                class="bg-gray-100 border-2 w-full p-4 rounded-lg @error('task') border-red-500 @enderror" value="{{ $comment->body }}">
                @error('body')
                    <div class="text-red-500 mt-2 text-sm">
                        {{ $message }}
                    </div>
                @enderror
            </div> 

            
            
            <div>
                <button type="submit" class="bg-blue-500 text-white px-4 py-3
                rounded font-medium w-full focus:outline-none hover:bg-indigo-600 rounded"> Edit </button>
            </div>           
        </form>
    </div>
</div>


@endsection