@extends('layouts.app')

@section('content')


<div class="flex justify-center">
    <div class="w-4/12 bg-white p-6 rounden-lg">
        <form action="{{ route('blockjobs.storeactivate', $blockjob) }}" method="post">
            @csrf
            <div>
                <label for="start_date">Start date:</label>
                <br>
                <input type="date" id="start_date" name="start_date" value="" min="" max="">            
            </div>
            <br><br>
            <div>
                <label for="deadline">Deadline:</label>
                <br>
                <input type="date" id="deadline" name="deadline" value="" min="" max="">            
            </div>
            <br><br>

            
            <div>
                <button type="submit" class="bg-blue-500 text-white px-4 py-3
                rounded font-medium w-full focus:outline-none hover:bg-indigo-600 rounded"> Activate </button>
            </div>           
        </form>
    </div>
</div>
@endsection
