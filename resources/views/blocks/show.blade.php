@extends('layouts.app')

@section('content')

    @if (session()->has('message'))
        <div id="alert" class="text-white px-6 py-4 border-0 rounded relative mb-4 bg-green-500 bg-opacity-75">
            <span class="inline-block align-middle mr-8">
                {{ session('message') }}
            </span>
            <button class="absolute bg-transparent text-2xl font-semibold leading-none right-0 top-0 mt-4 mr-6 outline-none focus:outline-none" onclick="document.getElementById('alert').remove();">
                <span>×</span>
            </button>
        </div>
    @endif
    
    <p class="text-lg text-center font-bold m-5">{{$block->project_name}}</p>
    <p class="text-lg text-center font-bold m-5">{{$block->name}}</p>
    <table class="rounded-t-lg m-5 w-5/6 mx-auto bg-white text-black-200 border-collapse">
    <tr class="text-left border-b border-black-300 bg-blue-500 bg-opacity-75 border text-left px-8 py-4">
        <th class="px-4 py-3 border border-black-600">#</th>
        <th class="px-4 py-3 border border-black-600">Job</th>
        <th class="px-4 py-3 border border-black-600">Subjob</th>
        <th class="px-4 py-3 border border-black-600">Status</th>
        <th class="px-4 py-3 border border-black-600">Etaj</th>
        <th class="px-4 py-3">Roles</th>
        <th class="px-4 py-3 border border-black-600">Start date</th>
        <th class="px-4 py-3 border border-black-600">Deadline</th>
        <th class="px-4 py-3 border border-black-600">Action</th>
    </tr>
    
    {{$userrole}}

    
    
    <br>
    @foreach ($blockjobs as $blockjob)
    

        
        
        <tr class="bg-white border-b border-black-600">
            <td class="px-3 py-2 border border-black-600">{{$blockjob->id}}</td>
            <td class="px-3 py-2 border border-black-600"> {{ $blockjob->job_name }}</td>
            <td class="px-3 py-2 border border-black-600"> {{ $blockjob->subjob_name }} </td>
            <td class="px-3 py-2 border border-black-600"> {{ $blockjob->status }}</td>
            <td class="px-3 py-2 border border-black-600"> @isset ($blockjob->etaj) {{ $blockjob->etaj }}  @endisset </td>
            <td class="px-3 py-2">  </td>
            <td class="px-3 py-2 border border-black-600"> @isset ($blockjob->start_date) {{ $blockjob->start_date->format('d-m-Y') }}  @endisset </td>
            <td class="px-3 py-2 border border-black-600"> @isset ($blockjob->deadline) {{ $blockjob->deadline->format('d-m-Y') }}  @endisset </td>
            <td class="px-3 py-2 border border-black-600"> <a href="{{ route ('blockjobs.show', $blockjob->id ) }}"> 
                <button type="button" class="bg-blue-500 text-white px-2 py-1
                    rounded font-medium w-full focus:outline-none hover:bg-indigo-600 rounded">Show</button> </a></td>
            
        </tr>    
        
    @endforeach
    
    </table>
    <div class="flex justify-center pl-20 pb-20 pr-20"> 
    {{ $blockjobs->links() }}
    </div>
@endsection
