
@extends('layouts.app')

@section('content')

    @if (session()->has('message'))
        <div id="alert" class="text-white px-6 py-4 border-0 rounded relative mb-4 bg-green-500 bg-opacity-75">
            <span class="inline-block align-middle mr-8">
                {{ session('message') }}
            </span>
            <button class="absolute bg-transparent text-2xl font-semibold leading-none right-0 top-0 mt-4 mr-6 outline-none focus:outline-none" onclick="document.getElementById('alert').remove();">
                <span>×</span>
            </button>
        </div>
    @endif

    @if (Auth::user()->isAdmin())
    <div class ="flex flex-wrap justify-end text-center mb-6"> 
        <a href="{{ route('projects.create')}}">
            <button class="flex mx-auto mt-6 text-white bg-blue-500 border-0 py-2 px-5 focus:outline-none hover:bg-indigo-600 rounded">Create Project</button>
        </a>
    </div>
    @endif
    <div class="flex justify-center p-5">
        <p class="text-lg text-center font-bold m-5"> {{$project->name}}</p>
    </div>
    

    <div>
    <div class="flex justify-center">
        <table class="shadow-lg bg-white">
            <thead>
                <tr>
                    <th class="bg-blue-500 bg-opacity-75 border text-left px-40 py-4">Name</th>            
                    <th class="bg-blue-500 bg-opacity-75 border text-left px-40 py-4">Actions</th>
                    
                </tr>
            </thead>
            <tbody>
                @foreach ($blocks as $block)
                <tr>            
                    <td class="border text-left px-40 py-4"> {{ $block->name }}</td>                    
                    <td class="border text-left px-40 py-4">                    

                        <a href="{{ route('blocks.show', $block->id) }}" class="inline-block justify-items-center">
                            <button type="button" class="bg-blue-500 text-white px-10 py-1
                                rounded font-medium w-full focus:outline-none hover:bg-indigo-600 rounded">Show</button>
                        </a>

                    </td>                    
                </tr>
                @endforeach     
            </tbody>
        </table>
    </div>

@endsection




