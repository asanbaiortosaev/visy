@extends('layouts.app')

@section('content')

    @if (session()->has('message'))
        <div id="alert" class="text-white px-6 py-4 border-0 rounded relative mb-4 bg-green-500 bg-opacity-75">
            <span class="inline-block align-middle mr-8">
                {{ session('message') }}
            </span>
            <button class="absolute bg-transparent text-2xl font-semibold leading-none right-0 top-0 mt-4 mr-6 outline-none focus:outline-none" onclick="document.getElementById('alert').remove();">
                <span>×</span>
            </button>
        </div>
    @endif

<div class="flex justify-center">
    <table class="shadow-lg bg-white">
    
    <thead>
    <tr>
        <th class="bg-blue-500 bg-opacity-75 border text-left px-8 py-4">Users</th>
        <th class="bg-blue-500 bg-opacity-75 border text-left px-8 py-4">Email</th>
        <th class="bg-blue-500 bg-opacity-75 border text-left px-8 py-4">Roles</th>
        <th class="bg-blue-500 bg-opacity-75 border text-left px-8 py-4">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($users as $user)
    <tr>
        <td class="border px-8 py-4">{{ $user->name }}</td>
        <td class="border px-8 py-4">{{ $user->email }}</td>
        <td class="border px-8 py-4">{{ $user->role->name }}</td>
        <td class="border px-8 py-4">
            <a href="{{ route ('admin.users.edit', $user->id ) }}"> 
            <button type="button" class="bg-blue-500 text-white px-4 py-3
                rounded font-medium w-full focus:outline-none hover:bg-indigo-600 rounded">Edit</button>
            </a>
        </td>
    </tr>
    </tbody>
    @endforeach     
    </table>
</div>


<div class="flex justify-center mt-6">
<a href="{{ route('admin.users.create') }}"><button type="button" class="bg-blue-500 text-white px-60 py-3
                rounded font-medium w-full focus:outline-none hover:bg-indigo-600 rounded">Create User</button> </a>
</div>



@endsection