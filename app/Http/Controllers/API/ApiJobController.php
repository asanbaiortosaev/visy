<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Job;
use DB;

class ApiJobController extends Controller
{
    
    public function index()
    {
        $jobs = Job::all();        
        return response()->json(['jobs' => $jobs]); 
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = \Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'roles' => ['required']      
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()], 400);
        }
        
        
        $job = new Job;
        $job->name = $data['name'];
        $job->roles = json_encode($data['roles']);            
        $job->save();   
        
        return response()->json(["message" => "Job created"], 201);
        
    }

    public function update(Request $request, $id)
    {
        $job = Job::findOrFail($id);
        $data = $request->all();
        $validator = \Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'roles' => ['required']      
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()], 400);
        }
        $job->name = $data['name']; 
        $job->roles = json_encode($data['roles']);            
        $job->update();        

        DB::table('subjobs')->where('job_id',$job->id)->update(
            ['job_name' => $job->name,]
            );
        
        DB::table('blockjobs')->where('job_id',$job->id)->update(
            ['job_name' => $job->name,]
            );

            return response()->json(["message" => "Job updated"], 201);
    }
    
}
