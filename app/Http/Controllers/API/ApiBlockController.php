<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Project;
use App\Models\Block;
use App\Models\Job;
use App\Models\Subjob;
use App\Models\Blockjob;
use DB;

class ApiBlockController extends Controller

{       
    public function index()
    {
        $blocks = Block::all();
        return response()->json(['blocks' => $blocks]); 
    }

    public function store(Request $request)
    {
        $project_id = $request->project_id;
        $project = Project::findOrFail($project_id);        

        $data = $request->all();
        $validator = \Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'etajnost' => ['required', 'integer'],                 
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()], 400);
        }
        
            $block = new Block;
            $block->name = $data['name'];
            $block->etajnost = $data['etajnost'];
            $block->project_id = $request->project_id;
            $block->project_name = $project->name;
            $block->save();
        
            $subjobs = Subjob::where('kratnost','1')->get();
            foreach ($subjobs as $subjob){
                $blockjob = new Blockjob;
                $blockjob->block_id  = $block->id;
                $blockjob->block_name  = $block->name;
                $blockjob->job_id  = $subjob->job_id;
                $blockjob->job_name  = $subjob->job_name;
                $blockjob->subjob_id  = $subjob->id;
                $blockjob->subjob_name  = $subjob->name;
                $blockjob->roles  = $subjob->roles;
                $blockjob->save();}

            for($i = 1; $i<=($block->etajnost);$i++) // assigning multiple subjobs for each floor
            {
                $subjobs = Subjob::where('kratnost','n')->get();
                foreach ($subjobs as $subjob){
                    $blockjob = new Blockjob;
                    $blockjob->block_id  = $block->id;
                    $blockjob->block_name  = $block->name;
                    $blockjob->job_id  = $subjob->job_id;
                    $blockjob->job_name  = $subjob->job_name;
                    $blockjob->subjob_id  = $subjob->id;
                    $blockjob->etaj  = $i;
                    $blockjob->subjob_name  = $subjob->name;
                    $blockjob->roles  = $subjob->roles;
                    $blockjob->save();}
            }

            // return redirect()->route('blocks.index');
        return response()->json(["message" => "block created"], 201);
    }

    public function update(Request $request, $id)
    {
        $block = Block::findOrFail($id);

        $data = $request->all();
        $validator = \Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'etajnost' => ['required', 'integer'],                 
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()], 400);
        }        
        
        $block->name = $data['name'];
        $block->etajnost = $data['etajnost'];            
        $block->update();

        DB::table('blockjobs')->where('block_id',$id)->update(
            ['block_name' => $block->name,]
            );

        // return redirect()->route('blocks.index');
        return response()->json(["message" => "block updated"], 201);
        }

        public function show($id)
        {
            $block = Block::where('id',$id)->first();
            $blockjobs = Blockjob::where('block_id', $id)->get();
            
            $userrole =(auth('api')->user()->role->name);
            
            return response()->json(['blockjobs' => $blockjobs]);
            
        }

        
    public function addsubjob(Request $request)
    {
        $block = Block::findOrFail($request->id);
        $job = Job::findOrFail($request->job_id);        
        $subjob = Subjob::findOrFail($request->subjob_id);
        $isExist = DB::table('blockjobs')->where('subjob_id',$subjob->id)->get();
        // check if subjob is already assigned to this block
        if ($isExist->isNotEmpty()) {
            return response()->json(["message" => "this subjob already exits in this block"], 404);   
        }

        else if ($subjob->kratnost == '1') 
            {
                $blockjob = new Blockjob;
                $blockjob->block_id  = $block->id;
                $blockjob->block_name  = $block->name;
                $blockjob->job_id  = $subjob->job_id;
                $blockjob->job_name  = $subjob->job_name;
                $blockjob->subjob_id  = $subjob->id;
                $blockjob->subjob_name  = $subjob->name;
                $blockjob->roles  = $subjob->roles;
                $blockjob->save();
            }
        
        else 
        for($i = 1; $i<=($block->etajnost);$i++)
            {
                $blockjob = new Blockjob;
                $blockjob->block_id  = $block->id;
                $blockjob->block_name  = $block->name;
                $blockjob->job_id  = $subjob->job_id;
                $blockjob->job_name  = $subjob->job_name;
                $blockjob->subjob_id  = $subjob->id;
                $blockjob->etaj  = $i;
                $blockjob->subjob_name  = $subjob->name;
                $blockjob->roles  = $subjob->roles;
                $blockjob->save();
            }
        
            return response()->json(["message" => "subjob added"], 201);
    }
    
}
