<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TaskComment;
use App\Models\Task;
use Auth;

class ApiTaskCommentController extends Controller
{
    
    public function store(Request $request, $id)
    {
        $task = Task::findOrFail($id);
        $data = $request->all();
        $validator = \Validator::make($request->all(), [
            'body' => ['string'],                           
        ]);

        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()], 400);
        }
        
        $taskcomment = new TaskComment;
        $taskcomment->body = $data['body'];
        $taskcomment->task_id = $task->id;
        $taskcomment->user = auth('api')->user()->name;
        $blockjob_id = $task->blockjob_id;
        $taskcomment->save();

        return response()->json(["message" => "taskcomment created"], 201);
        // return redirect()->route('blockjobs.show', ['id'=> $blockjob_id]);
    }

    public function update(Request $request, $id)
    {
        $taskcomment = TaskComment::findOrFail($id);
        $task = Task::where('id',$taskcomment->task->id)->first();
        
        $data = $request->all();
        $validator = \Validator::make($request->all(), [
            'body' => ['string'],                           
        ]);
        if ($validator->fails()) {
            return response()->json(["errors" => $validator->errors()], 400);
        }

        $taskcomment->body = $data['body'];
        $blockjob_id = $task->blockjob_id;
        $taskcomment->update();

        return response()->json(["message" => "taskcomment updated"], 201);
    }
}
