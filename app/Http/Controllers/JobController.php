<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Job;
use App\Models\Role;
use DB;

class JobController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','admin']);
        // $this->middleware(['can:manage-users']);
    }
    
    public function index()
    {
        $jobs = Job::all();
        return view('jobs.index',compact('jobs'));
    }

    public function create()
    {
        $roles = Role::all();
        return view('jobs.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $data = request()->validate([
            'name' => ['required', 'string', 'max:255'],
            'roles' => ['required']     
        ]);
        // dd($request->roles);
        $job = new Job;
        $job->name = $data['name'];
        $job->roles = json_encode($data['roles']);            
        $job->save();   
        session()->flash('message', 'Job '.$job->name. '  has been created');
        return redirect()->route('jobs.index');
    }

    public function edit($id)
    {
        
        $roles = Role::all();
        $job = Job::findOrFail($id);    
        $var = json_decode($job->roles);  //$job->roles returns string, used json_decode to change it to array
        
        return view ('jobs.edit', compact('job','roles','var'));
    }

    public function update(Request $request, $id)
    {
        $job = Job::findOrFail($id);
        $data = request()->validate([
            'name' => ['required', 'string', 'max:255'], 
            'roles' => ['required']       
        ]);
        $job->name = $data['name']; 
        $job->roles = json_encode($data['roles']);            
        $job->update();
        

        DB::table('subjobs')->where('job_id',$job->id)->update(
            ['job_name' => $job->name,
            'roles' => $job->roles,
            ]
            );
        
        DB::table('blockjobs')->where('job_id',$job->id)->update(
            ['job_name' => $job->name,
            'roles' => $job->roles,
            ]
            );
        
        session()->flash('message', 'Job '.$job->name. '  has been updated');
        return redirect()->route('jobs.index');
    }
}
