<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use DB;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    
    public function index()
    {        
        $projects = Project::all();
        // $q = $projects->blocks[0]->name;
        // dd($q);

        return view('projects.index',compact('projects'));
    }

    public function create()
    {
        return view('projects.create');
    }

    public function store(Request $request)
    {
        $data = request()->validate([
            'name' => ['required', 'string', 'max:255'],      
        ]);
        $project = new Project;
        $project->name = $data['name'];            
        $project->save();
        session()->flash('message', 'Project '.$project->name. '  has been created');
        return redirect()->route('projects.index');
    
    }

    public function edit($id)
    {
        $project = Project::findOrFail($id);
        return view ('projects.edit', compact('project'));
    }

    public function update(Request $request, $id)
    {
        $project = Project::findOrFail($id);
        $data = request()->validate([
            'name' => ['required', 'string', 'max:255'],      
        ]);
        $project->name = $data['name'];            
        $project->update();

        DB::table('blocks')->where('project_id',$project->id)->update(
            ['project_name' => $project->name,]
            );
        session()->flash('message', 'Project '.$project->name. '  has been updated');
        return redirect()->route('projects.index');
    }

    public function show($id)
    {
        $project = Project::findOrFail($id);
        $blocks = $project->blocks()->get();
        
        return view('projects.show',compact('project','blocks'));
    }
}
