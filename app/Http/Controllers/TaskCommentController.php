<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\TaskComment;
use Auth;

class TaskCommentController extends Controller
{
    public function create($id)
    {
        $task = Task::findOrFail($id);
        return view ('taskcomments.create',compact('task'));
    }

    public function store(Request $request, $id)
    {
        $task = Task::findOrFail($id);
        $data = request()->validate([
            'body' => ['string'],      
        ]);
        $taskcomment = new TaskComment;
        $taskcomment->body = $data['body'];
        $taskcomment->task_id = $task->id;
        $taskcomment->user = Auth::user()->name;
        $blockjob_id = $task->blockjob_id;
        $taskcomment->save();

        return redirect()->route('blockjobs.show', ['id'=> $blockjob_id])
        ->with('message', 'Comment created');
    }

    public function edit($id)
    {
        $comment = TaskComment::findOrFail($id);
        return view('taskcomments.edit',compact('comment'));
    }

    public function update(Request $request, $id)
    {
        $taskcomment = TaskComment::findOrFail($id);
        $task = Task::where('id',$taskcomment->task->id)->first();
        $data = request()->validate([
            'body' => ['string'],      
        ]);
        $taskcomment->body = $data['body'];
        $blockjob_id = $task->blockjob_id;
        $taskcomment->update();

        return redirect()->route('blockjobs.show', ['id'=> $blockjob_id])
        ->with('message', 'Comment updated');
    }
    
}

