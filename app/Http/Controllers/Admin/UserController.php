<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;


class UserController extends Controller
{   
    public function __construct()
    {
        $this->middleware(['auth','admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('role')->where('id','!=', 1)->get();
        // dd($users);
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('name','!=','admin')->get();
        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'email' => ['required', 'email', 'max:255', 'unique:users'],
            'name' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'roles' =>['required']
            
        ]);
        
        // dd($request->roles);
        $user = new User();
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->name = $data['name'];
        $user->role_id = implode(' ', $request->roles);
        
        
        if($user->save()){
            session()->flash('message', 'User '.$user->name. '  has been created');
        }
        else{
            session()->flash('message','There was an error could not create the user');
        }
        
        
        return redirect (route('admin.users.index'));
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        

        $roles = Role::where('name','!=','admin')->get();
        return view('admin.users.edit',compact('roles','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $data = request()->validate([
            'name' => ['required', 'string', 'max:255'],            
            'email' => ['required', Rule::unique('users')->ignore($user->id)],
            'roles' =>['required']
            
        ]);
        $user->name = $data['name'];        
        $user->email = $data['email'];
        $user->role_id = implode(' ', $request->roles);
        
        
        if ($request->filled('password'))
            {$request->validate(['password' => 'min:8', 'confirmed']);
                $user->password = Hash::make($request['password']);}
            
            if($user->update()){
                session()->flash('message', 'User '.$user->name. '  has been updated');
            }
            else{
                $request->session()->flash('error','There was an error updating the user');
            } 
            
            return redirect()->route('admin.users.index');
        
    }

}