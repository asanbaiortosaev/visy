<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blockjob;
use App\Models\Task;
use Auth;

class TaskController extends Controller
{
    public function create($id)
    {
        $blockjob = Blockjob::findOrFail($id);
        return view ('tasks.create', compact('blockjob'));
    }

    public function store(Request $request, $id)
    {
        $blockjob = Blockjob::findOrFail($id);
        // dd($blockjob);
        $data = request()->validate([
            'body' => ['string'],      
        ]);
        // $var = Auth::user()->role->name;
        // dd($var);
        $task = new Task;
        $task->blockjob_id = $blockjob->id;
        $task->body = $data['body'];
        $task->user = Auth::user()->name;
        
        $task->save();
        
        // return redirect()->route('blockjobs.show',compact('blockjob'));
        return redirect()->route('blockjobs.show', ['id'=> $id])
        ->with('message', 'Taks created');
        
    }

    public function edit($id)
    {
        $task = Task::findOrFail($id);
        $blockjob_id = $task->blockjob_id;
        $blockjob = Blockjob::findOrFail($blockjob_id);

        return view ('tasks.edit', compact('task','blockjob'));
    }

    public function update(Request $request, $id)
    {
        $task = Task::findOrFail($id);
        $data = request()->validate([
            'body' => ['string'],      
        ]);
        $task->body = $data['body'];
        $task->update();
        $blockjob_id = $task->blockjob_id;     
        
        return redirect()->route('blockjobs.show', ['id'=> $blockjob_id])
        ->with('message', 'Taks updated');
    }

}
