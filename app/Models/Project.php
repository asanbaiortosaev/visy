<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Block;

class Project extends Model
{
    use HasFactory;

    public function blocks()
    {
        return $this->HasMany(Block::class);
        
    }
}
