<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Task;

class Blockjob extends Model
{
    use HasFactory;

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    protected $fillable = [
        'block_id',
        'block_name',
        'job_id',
        'job_name',
        'subjob_id',
        'subjob_name',
        'roles',
        'status',
        'etaj',
        'start_date',
        'deadline',
        'finish_date',
        
    ];

    protected $casts = [
        'start_date' => 'datetime',
        'deadline' => 'datetime',
        'finish_date' => 'datetime',
    ];
}
