<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Task;

class TaskComment extends Model
{
    use HasFactory;

    protected $fillable = [
        'taskcomment',
        'task_id',
    ];

    public function task()
    {
        return $this->belongsTo(Task::class);
    }

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',        
    ];
}
