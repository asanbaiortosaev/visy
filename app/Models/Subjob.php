<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Job;
use App\Models\Role;
use App\Models\Block;

class Subjob extends Model
{
    use HasFactory;

    public function jobs()
    {
        return $this->belongsTo(Job::class);
    }

    

    
}
