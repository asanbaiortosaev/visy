<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Blockjob;
use App\Models\TaskComment;

class Task extends Model
{
    use HasFactory;

    protected $fillable= [
        'user',
        'body', 
        'blockjob_id',
    ];

    public function blockjobs()
    {
        return $this->belongsTo(Blockjob::class);
    }


    public function comments()
    {
        return $this->hasMany(TaskComment::class);
    }

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',        
    ];
}
